# ubuntu-rust-test-c

A test Docker image based on Ubuntu to build and test a basic Rust program (using `COPY`).

Program sources are copied into the `build` container and program is built from there when building the `build` container.
Program binary is then copied from the `build` container into the `test` container and run from there.

## Setup

* Install docker: https://docs.docker.com/engine/installation/
* Build the docker image: `make build`
* Run the docker image: `make run`
